﻿using Microsoft.AspNetCore.Mvc;

namespace helloworld.Controllers
{
    public class Hello
    {
        public string Message { get; set; }
    }

    [Route("{*url}")]
    [ApiController]
    public class HelloWorldController : ControllerBase
    {
        [HttpGet]
        public ActionResult<Hello> Get()
        {
            var hello = new Hello
            {
                Message = "Hello world!"
            };
            return Ok(hello);
        }
    }
}
